package com.company.Kolokwium1;

import java.util.Scanner;

public class Zadanie1Kolokwium1 {
    public static void Liczby() {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] liczby = new int[3];
        int[] listaliczbwczytanych = new int[n];
        liczby[0] = 0;
        liczby[1] = 0;
        liczby[2] = 0;
        for (int i =0 ;i < n ; i++)
        {
            listaliczbwczytanych[i]=sc.nextInt();
        }
        for (int i =0 ;i < n ; i++)
        {
            if(listaliczbwczytanych[i]>5)
            {
                liczby[0]++;
            }
            if(listaliczbwczytanych[i]<-5)
            {
                liczby[1]++;
            }
            if(listaliczbwczytanych[i]==-5)
            {
                liczby[2]++;
            }
        }
        System.out.println("Liczby wieksze od 5 :" + liczby[0]);
        System.out.println("Liczby mniejsze od -5 :" + liczby[1]);
        System.out.println("Liczby rowne -5 :" + liczby[2]);
    }
}