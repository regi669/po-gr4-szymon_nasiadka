package com.company.Kolokwium1;


public class Zadanie2Kolokwium1 {
    public static String delete(String str, char c) {
        String buffer ="";
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)=='a'||str.charAt(i)=='e'||str.charAt(i)=='i'||str.charAt(i)=='o'||str.charAt(i)=='u'||str.charAt(i)=='y'||str.charAt(i)=='ą'||str.charAt(i)=='ę'||str.charAt(i)=='ó'||str.charAt(i)=='A'||str.charAt(i)=='E'||str.charAt(i)=='I'||str.charAt(i)=='O'||str.charAt(i)=='U'||str.charAt(i)=='Y'||str.charAt(i)=='Ą'||str.charAt(i)=='Ę'||str.charAt(i)=='Ó'){
                buffer += c;
            }
           else
           {
                buffer += str.charAt(i);
           }
        }
        return buffer;
    }
}