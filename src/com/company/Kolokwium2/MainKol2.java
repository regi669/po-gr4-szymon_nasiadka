package com.company.Kolokwium2;

import com.company.Kolokwium2.pl.imiajd.nasiadka.*;

import java.time.LocalDate;
import java.util.*;

public class MainKol2 {
    public static void main(String args[]) {

        System.out.println("Zadanie1");

        ArrayList<Autor> grupa = new ArrayList<>();
        grupa.add(0, new Autor("Marek","marek123@kappa.kappa",'m'));
        grupa.add(1, new Autor("Marek","marek312@kappa.kappa",'m'));
        grupa.add(2, new Autor("Kasia","Kasia@kappa.kappa",'k'));
        grupa.add(3, new Autor("Basia","Basia@kappa.kappa",'k'));
        System.out.println("Przed Sortowaniem :");
        wypisz(grupa);
        Collections.sort(grupa);
        System.out.println("Po Sortowaniu :");
        wypisz(grupa);

        System.out.println("\n\n");

        ArrayList<Ksiazka> Listaksiazek = new ArrayList<>();
        Listaksiazek.add(0, new Ksiazka("Jan Fesola",grupa.get(0),100.0));
        Listaksiazek.add(1, new Ksiazka("Bonifacy",grupa.get(0),50.0));
        Listaksiazek.add(2, new Ksiazka("Głodówka w lesie węży",grupa.get(1),100.0));
        Listaksiazek.add(3, new Ksiazka("Marcin Dubiel na Zakręcie",grupa.get(2),100.0));
        System.out.println("Przed Sortowaniem :");
        wypisz(Listaksiazek);
        Collections.sort(Listaksiazek);
        System.out.println("Po Sortowaniu :");
        wypisz(Listaksiazek);
        System.out.println("\nTest Zadania2\n");
        LinkedList<Ksiazka> ksiazki = new LinkedList<Ksiazka>();
        ksiazki.add(0, new Ksiazka("Jan Fesola",grupa.get(0),100.0));
        ksiazki.add(1, new Ksiazka("Bonifacy",grupa.get(0),50.0));
        ksiazki.add(2, new Ksiazka("Głodówka w lesie węży",grupa.get(1),100.0));
        ksiazki.add(3, new Ksiazka("Marcin Dubiel na Zakręcie",grupa.get(2),100.0));
        System.out.println("Przed Usuwaniem");
        System.out.print(ksiazki);
        System.out.println("Po Usuwaniu");
        RedukujKol2.redukuj(ksiazki, 3);
    }
    public static <T> void wypisz(ArrayList<T> a)
    {
        for(int i = 0 ; i < a.size() ; i++)
        {
            System.out.print(a.get(i).toString());
        }
    }
}