package com.company.Kolokwium2;

import java.util.LinkedList;

public class RedukujKol2 {
    public static <T> void redukuj(LinkedList<T> books, int n) {
        for (int i = n-1; i < books.size(); i += (n - 1)) {
            books.remove(i);
        }
        System.out.println(books);
    }
}
