package com.company.labolatorium1;

public class Zadanie11
{
    public static void Zadanie11test()
    {
        System.out.println("Nic dwa razy się nie zda\u00ADrza\n" +
                "i nie zda\u00ADrzy. Z tej przy\u00ADczy\u00ADny\n" +
                "zro\u00ADdzi\u00ADli\u00ADśmy się bez wpra\u00ADwy\n" +
                "i po\u00ADmrze\u00ADmy bez ru\u00ADty\u00ADny.\n" +
                "\n" +
                "Choć\u00ADby\u00ADśmy ucznia\u00ADmi byli\n" +
                "naj\u00ADtęp\u00ADszy\u00ADmi w szko\u00ADle świa\u00ADta,\n" +
                "nie bę\u00ADdzie\u00ADmy re\u00ADpe\u00ADto\u00ADwać\n" +
                "żad\u00ADnej zimy ani lata.\n" +
                "\n" +
                "Żaden dzień się nie po\u00ADwtó\u00ADrzy,\n" +
                "nie ma dwóch po\u00ADdob\u00ADnych nocy,\n" +
                "dwóch tych sa\u00ADmych po\u00ADca\u00ADłun\u00ADków,\n" +
                "dwóch jed\u00ADna\u00ADkich spoj\u00ADrzeń w oczy.\n" +
                "\n" +
                "Wczo\u00ADraj, kie\u00ADdy two\u00ADje imię\n" +
                "ktoś wy\u00ADmó\u00ADwił przy mnie gło\u00ADśno,\n" +
                "tak mi było, jak\u00ADby róża\n" +
                "przez otwar\u00ADte wpa\u00ADdła okno.\n" +
                "\n" +
                "Dziś, kie\u00ADdy je\u00ADste\u00ADśmy ra\u00ADzem,\n" +
                "od\u00ADwró\u00ADci\u00ADłam twarz ku ścia\u00ADnie.\n" +
                "Róża? Jak wy\u00ADglą\u00ADda róża?\n" +
                "Czy to kwiat? A może ka\u00ADmień?\n" +
                "\n" +
                "Cze\u00ADmu ty się, zła go\u00ADdzi\u00ADno,\n" +
                "z nie\u00ADpo\u00ADtrzeb\u00ADnym mie\u00ADszasz lę\u00ADkiem?\n" +
                "Je\u00ADsteś - a więc mu\u00ADsisz mi\u00ADnąć.\n" +
                "Mi\u00ADniesz - a więc to jest pięk\u00ADne.\n" +
                "\n" +
                "Uśmiech\u00ADnię\u00ADci, współ\u00ADo\u00ADbję\u00ADci\n" +
                "spró\u00ADbu\u00ADje\u00ADmy szu\u00ADkać zgo\u00ADdy,\n" +
                "choć róż\u00ADni\u00ADmy się od sie\u00ADbie\n" +
                "jak dwie kro\u00ADple czy\u00ADstej wody.\n");
    }
}