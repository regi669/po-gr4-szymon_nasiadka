package com.company.labolatorium12;

import java.util.*;

public class Zad8Lab12 {
    public static <E extends Iterable<?>> void print (E o ){
        Iterator <?> iterator = o.iterator();
        while(iterator.hasNext()) {
            System.out.println( iterator.next());
            if(iterator.hasNext()){
                System.out.println(", ");
            }
        }
    }
}
