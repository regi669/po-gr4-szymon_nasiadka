package com.company.labolatorium3;
import java.util.Scanner;
import  java.util.Random;

public class MainLab3
{
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int m,n,k;
        n=sc.nextInt();
        int[] tab = new int[n];
        ///zadanie 1
        Zadanie1Lab3.Zadania(tab);
        ///zadanie 2
        n=sc.nextInt();
        int lewy=sc.nextInt(), prawy=sc.nextInt();
        Zadanie2Lab3.rand(tab, n,-999,999);
        System.out.print("a) parzyste : ");
        System.out.print(Zadanie2Lab3.ileParzystych(tab));
        System.out.print("\na) nieparzyste : ");
        System.out.print(Zadanie2Lab3.ileNieparzystych(tab));
        System.out.print("\nb) dodatnie : ");
        System.out.print(Zadanie2Lab3.ileDodatnich(tab));
        System.out.print("\nb) ujemne : ");
        System.out.print(Zadanie2Lab3.ileUjemnych(tab));
        System.out.print("\nb) zera : ");
        System.out.print(Zadanie2Lab3.ileZerowych(tab));
        System.out.print("\nd) suma dodatnich : ");
        System.out.print(Zadanie2Lab3.sumaDotatnich(tab));
        System.out.print("\nd) suma ujemnych : ");
        System.out.print(Zadanie2Lab3.sumaUjemnych(tab));
        System.out.print("\nc) najwieksza liczba to : ");
        Zadanie2Lab3.ileMaksymalnych(tab);
        System.out.print("\ne) najdluzszy element dodatni : ");
        System.out.print(Zadanie2Lab3.dlugoscMaksymalnegoCiaguDodatnich(tab));
        System.out.println("\nf) Przemiana : ");
        Zadanie2Lab3.signum(tab);
        System.out.println("g) Odwrocenie : \n");
        Zadanie2Lab3.odwrocFragment(tab,lewy,prawy);
        ///zadanie 3
        m=sc.nextInt();
        n=sc.nextInt();
        k=sc.nextInt();
        Zadanie3Lab3.wlacz(m,n,k);
    }
}