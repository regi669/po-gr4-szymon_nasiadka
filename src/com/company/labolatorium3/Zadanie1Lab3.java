package com.company.labolatorium3;
import java.util.Scanner;
import  java.util.Random;

public class Zadanie1Lab3 {
    public static void Zadania(int tab[]) {
        int wartoscmin = -999, wartoscmax = 999;
        Random random=new Random();
        int n = tab.length;
        for(int i=0;i<n; i++)
        {
            tab[i]=random.nextInt(wartoscmax-wartoscmin)+wartoscmin;
        }
        Scanner sc = new Scanner(System.in);
        int p = 0;
        int np = 0;
        int dodatnie = 0;
        int ujemne = 0;
        int zerowe = 0;
        int najwiekszy = tab[0] - 1;
        int ilosc = 0;
        int sumadodatnich = 0;
        int sumaujemnych = 0;
        int ciagdotliczb = 0;
        int licznik = 0;
        int prawy = sc.nextInt();
        int lewy = sc.nextInt();
        int[] tab2 = new int[n];
        for (int i = 0; i < n; i++) {
            tab2[i]=tab[i];
        }
        for (int i = 0; i < n; i++) {
            if (tab[i] % 2 == 0) {
                p++;
            } else {
                np++;
            }
            if (tab[i] > 0) {
                dodatnie++;
                sumadodatnich += tab[i];
                licznik++;
            } else if (tab[i] == 0) {
                zerowe++;
            } else {
                ujemne++;
                sumaujemnych += tab[i];
            }
            if (tab[i] < 1) {
                if (licznik > ciagdotliczb) {
                    ciagdotliczb = licznik;
                    licznik = 0;
                } else
                    licznik = 0;
            }
            if (tab[i] > najwiekszy) {
                najwiekszy = tab[i];
                ilosc = 1;
            } else if (tab[i] == najwiekszy) {
                ilosc++;
            }
        }
        for (int i = 0; i < n; i++) {
            if (tab[i] >= 0) {
                tab2[i] = 1;
            } else if (tab[i] < 0) {
                tab2[i] = -1;
            }
        }
        System.out.print("a) parzyste : ");
        System.out.print(p);
        System.out.print("\na) nieparzyste : ");
        System.out.print(np);
        System.out.print("\nb) dodatnie : ");
        System.out.print(dodatnie);
        System.out.print("\nb) ujemne : ");
        System.out.print(ujemne);
        System.out.print("\nb) zera : ");
        System.out.print(zerowe);
        System.out.print("\nc) najwieksza liczba to : ");
        System.out.print(najwiekszy);
        System.out.print("\nWystepuje razy : ");
        System.out.print(ilosc);
        System.out.print("\nd) suma dodatnich : ");
        System.out.print(sumadodatnich);
        System.out.print("\nd) suma ujemnych : ");
        System.out.print(sumaujemnych);
        System.out.print("\ne) najdluzszy element dodatni : ");
        System.out.print(ciagdotliczb);
        System.out.println("\nf) Przemiana : ");
        for (int i = 0; i < n; i++) {
            System.out.println(tab2[i]);
        }
        for (int i = 0; i < (prawy - lewy) / 2; i++) {
            int pomoc = tab[prawy - i];
            tab[prawy - i] = tab[lewy + i];
            tab[lewy + i] = pomoc;
        }
        System.out.println("g) Odwrocenie : \n");
        for (int i = 0; i < n; i++) {
            System.out.println(tab[i]);
        }
    }
}