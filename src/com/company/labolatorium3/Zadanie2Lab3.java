package com.company.labolatorium3;
import java.util.Scanner;
import  java.util.Random;

public class Zadanie2Lab3 {
    //a
    public static int ileNieparzystych(int tab[])
    {
        int nieparzyste=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if(tab[i]%2!=0)
                nieparzyste++;
        }
        return nieparzyste;
    }
    public static int ileParzystych(int tab[])
    {
        int parzyste=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if(tab[i]%2==0)
                parzyste++;
        }
        return  parzyste;
    }
    //b
    public static int ileDodatnich(int tab[])
    {
        int plus=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if(tab[i]>0)
                plus++;

        }
        return plus;
    }
    public static int ileUjemnych(int tab[])
    {
        int  minus=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if(tab[i]<0)
                minus++;
        }
        return minus;
    }
    public static int ileZerowych(int tab[])
    {
        int zero=0, plus=0, minus=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if(tab[i]==0)
                zero++;
        }
        return zero;
    }
    //c
    public static void ileMaksymalnych(int tab[])
    {

        int n=tab.length;
        int najwiekszy=tab[0];
        for(int i=0; i<n; i++)
        {
            if(tab[i]>najwiekszy)
                najwiekszy=tab[i];
        }
        int ilosc=0;
        for(int i=0; i<n; i++)
        {
            if(tab[i]==najwiekszy)
            {
                ilosc++;
            }
        }
        System.out.println(ilosc);
    }
    //d
    public static int  sumaDotatnich(int tab[])
    {
        int plus=0;
        int n=tab.length;
        for(int i=0; i<n; i++) {
            if (tab[i] > 0)
                plus += tab[i];
        }
        return plus;
    }
    public static int sumaUjemnych(int tab[])
    {
        int plus=0;
        int n=tab.length;
        for(int i=0; i<n; i++)
        {
            if (tab[i]<0)
                plus+=tab[i];
        }
        return plus;
    }
    //e
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[])
    {
        int n=tab.length;
        int nd=0;
        int d=0;
        for(int i=0; i<n; i++)
        {
            if(tab[i]<=0)
            {
                if (d > nd)
                {
                    nd=d;
                }
                d=0;
            }
            else
                d++;
        }
        if (d>nd) {
            nd=0;
        }
        return nd;
    }
    //f
    public static void signum(int tab[])
    {
        int n=tab.length;
        int[] tab2 = new int[n];
        for(int i=0; i<n;i++)
        {
            if(tab[i]>0)
            {
                tab2[i]=1;
            }
            else if(tab[i]<0)
            {
                tab2[i]=-1;
            }
        }
        for(int i=0;i<n;i++)
            System.out.println(tab2[i]);
    }
    //g
    public static void odwrocFragment(int tab[], int lewy, int prawy) {
        int n=tab.length;
        if (lewy > prawy){
            int h=prawy;
            prawy=lewy;
            lewy=h;
        }
        for(int i=0; i<(lewy-prawy)/2; i++)
        {
            int h=tab[prawy-i];
            tab[prawy-i]=tab[lewy+i];
            tab[lewy+i]=h;
        }
        for(int i=0; i<n; i++)
        {
            System.out.println(tab[i]);
        }
    }
    public static void rand(int tab[], int n, int min, int max)
    {
        Random random=new Random();
        for(int i=0;i<n; i++)
        {
            tab[i]=random.nextInt(max-min)+min;
        }
    }
}