package com.company.labolatorium3;
import java.util.Scanner;
import  java.util.Random;

public class Zadanie3Lab3 {
    public static void wlacz(int m, int n, int k){
        Random r = new Random();
        int[][] tab1 = new int[m][];
        for(int i = 0; i<m; i++) {
            tab1[i] = new int[n];
        }

        int[][] tab2 = new int[n][];
        for(int i = 0; i<n; i++) {
            tab2[i] = new int[k];
        }

        int[][] tab3 = new int[m][];
        for(int i = 0; i<m; i++) {
            tab3[i] = new int[k];
        }

        for(int i = 0; i<m; i++) {
            for (int j = 0; j < n; j++) {
                tab1[i][j] = r.nextInt(10);
                System.out.printf("%d ", tab1[i][j]);
            }
            System.out.printf("\n");
        }
        System.out.printf("\n");

        for(int i = 0; i<n; i++) {
            for (int j = 0; j < k; j++) {
                tab2[i][j] = r.nextInt(10);
                System.out.printf("%d ", tab2[i][j]);
            }
            System.out.printf("\n");
        }
        System.out.printf("\n");

        int suma;
        for(int i = 0; i<m; i++) {
            for (int j = 0; j < k; j++) {
                suma = 0;
                for(int l = 0; l<n; l++)
                    suma = suma+tab1[i][l]*tab2[l][j];
                tab3[i][j] = suma;
                System.out.printf("%d ", tab3[i][j]);
            }
            System.out.printf("\n");
        }
    }
}