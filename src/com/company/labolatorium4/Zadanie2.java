package com.company.labolatorium4;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Zadanie2 {
    public static int fileCountChar(String filename, char symbol){
        try {
            int counter = 0;
            File file = new File(filename);
            String read;
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                read = scanner.nextLine();
                counter = counter + Zadanie1.countChar(read, symbol);
            }
            return counter;
        } catch (FileNotFoundException error) {
            System.out.println("Error");
            error.printStackTrace();
            return 0;
        }

    }
}