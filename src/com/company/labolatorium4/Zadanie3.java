package com.company.labolatorium4;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Zadanie3 {
    public static int fileCountSubStr(String filename, String subStr){
        try {
            int licznik = 0;
            File file = new File(filename);
            String read;
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                read = scanner.nextLine();
                licznik = licznik + Zadanie1.countSubStr(read, subStr);
            }
            return licznik;
        } catch (FileNotFoundException error) {
            System.out.println("Error");
            error.printStackTrace();
            return 0;
        }

    }
}