package com.company.labolatorium4;

import java.math.BigInteger;

public class Zadanie4 {
    public static void cheesboard(int n){
        BigInteger c = new BigInteger("0");
        BigInteger p = new BigInteger("2");
        for(int i = 0; i<n*n; i++)
            c = c.add(p.pow(i));
        System.out.println(c);
    }
}