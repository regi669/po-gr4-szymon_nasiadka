package com.company.labolatorium4;

import java.math.BigDecimal;

public class Zadanie5 {
    public static void kapital(double k, double p, int n){
        BigDecimal kap = new BigDecimal(Double.toString(k)).setScale(2);
        BigDecimal procent = new BigDecimal(Double.toString(p/100.0+1)).setScale(2);
        procent = procent.pow(n);
        kap = kap.multiply(procent);

        System.out.println(kap);
    }
}