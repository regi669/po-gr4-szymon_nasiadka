package com.company.labolatorium5;

import java.util.ArrayList;

public class MainLab5 {

    public static void main(String[] args) {
        ArrayList<Integer> jeden = new ArrayList<Integer>();
        jeden.add(5);
        jeden.add(3);
        jeden.add(2);
        jeden.add(1);
        jeden.add(4);
        ArrayList<Integer> dwa = new ArrayList<Integer>();
        dwa.add(1);
        dwa.add(2);
        dwa.add(3);
        dwa.add(4);
        dwa.add(5);
        dwa.add(6);
        //Zadanie1
        ArrayList<Integer> trzy = Zadanie1Lab5.append(jeden,dwa);
        System.out.println(trzy);
        //Zadanie2
        ArrayList<Integer> merged = Zadanie2Lab5.merge(jeden,dwa);
        System.out.println(merged);
        //Zadanie3
        ArrayList<Integer> mergedsorted = Zadanie3Lab5.mergeSorted(jeden,dwa);
        System.out.println(mergedsorted);
        //Zadanie4
        ArrayList<Integer> odwrocona = Zadanie4Lab5.reversed(jeden);
        System.out.println(odwrocona);
        //Zadanie5
        System.out.println(jeden);
        Zadanie5Lab5.reverse(jeden);
        System.out.println(jeden);
    }
}