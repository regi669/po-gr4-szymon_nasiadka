package com.company.labolatorium5;
import java.util.ArrayList;

public class Zadanie2Lab5 {
    public static ArrayList<Integer>  merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> Nowa = new ArrayList<Integer>();
        int rozmiara = a.size();
        int rozmiarb = b.size();
        if(rozmiara>rozmiarb)
        {
            for(int i=0 ; i < rozmiarb ; i++)
            {
                Nowa.add(a.get(i));
                Nowa.add(b.get(i));
            }
            for(int j=rozmiarb ; j < rozmiara ; j++)
            {
                Nowa.add(a.get(j));
            }
        }
        else if(rozmiara<rozmiarb)
        {
            for(int i=0 ; i < rozmiara ; i++)
            {
                Nowa.add(a.get(i));
                Nowa.add(b.get(i));
            }
            for(int j=rozmiara ; j < rozmiarb ; j++)
            {
                Nowa.add(b.get(j));
            }
        }
        else
        {
            for(int i=0 ; i < rozmiara ; i++)
            {
                Nowa.add(a.get(i));
                Nowa.add(b.get(i));
            }
        }
        return Nowa;
    }
}