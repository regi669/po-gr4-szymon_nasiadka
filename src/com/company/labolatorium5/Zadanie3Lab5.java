package com.company.labolatorium5;
import java.util.ArrayList;

public class Zadanie3Lab5 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> Pomoc = new ArrayList<Integer>();
        ArrayList<Integer> Nowa = new ArrayList<Integer>();
        int rozmiara = a.size();
        int rozmiarb = b.size();
        for(int i=0 ; i < rozmiara ; i++)
        {
            Pomoc.add(a.get(i));
        }
        for(int j=0 ; j < rozmiarb ; j++)
        {
            Pomoc.add(b.get(j));
        }
        int sizepomocy = Pomoc.size();
        for(int i=0 ; i < sizepomocy ; i++)
        {
            int min = minimum(Pomoc);
            Nowa.add(min);
            Pomoc.remove(Pomoc.indexOf(min));
        }
        return Nowa;
    }
    public static int minimum (ArrayList<Integer> a){
        int min = a.get(0);
        for(int i=0 ; i < a.size() ; i++)
        {
            if(a.get(i) < min)
            {
                min = a.get(i);
            }
        }
        return min;
    }
}