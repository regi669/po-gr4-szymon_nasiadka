package com.company.labolatorium5;
import java.util.ArrayList;

public class Zadanie4Lab5 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> Nowa = new ArrayList<Integer>();
        for(int i=a.size()-1 ; i >= 0 ; i--)
        {
            Nowa.add(a.get(i));
        }
        return Nowa;
    }
}