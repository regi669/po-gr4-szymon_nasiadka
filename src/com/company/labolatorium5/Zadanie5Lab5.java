package com.company.labolatorium5;
import java.util.ArrayList;

public class Zadanie5Lab5 {
    public static void reverse(ArrayList<Integer> a){
        int pomoc;
        for(int i=0 ; i < a.size()/2 ; i++)
        {
            pomoc = a.get(i);
            a.set(i, a.get(a.size()-1-i));
            a.set(a.size()-i-1, pomoc);
        }
    }
}