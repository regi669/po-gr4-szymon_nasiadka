package com.company.labolatorium6;



public class IntegerSet {
    boolean[] tablica;

    public IntegerSet() {
        this.tablica = new boolean[100];
    }
    public void wypiszTab() {
        for(int i = 0 ; i<100 ; i++) {
            System.out.print(this.tablica[i]);
            System.out.print(" ");
        }
        System.out.print("\n");
    }
    public static IntegerSet union(IntegerSet a, IntegerSet b) {
        IntegerSet suma = new IntegerSet();
        for(int i = 0 ; i<100 ; i++) {
            if (a.tablica[i] || b.tablica[i]) {
                suma.tablica[i] = true;
            }
        }
        return suma;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b) {
        IntegerSet ints = new IntegerSet();
        for(int i = 0 ; i<100 ; i++) {
            if (a.tablica[i] && b.tablica[i]) {
                ints.tablica[i] = true;
            }
        }
        return ints;
    }
    public void insertElement(int n)
    {
        this.tablica[n-1]=true;
    }
    public void deleteElement(int n)
    {
        this.tablica[n-1]=false;
    }

    @Override
    public String toString()
    {
        StringBuffer indexy = new StringBuffer();
        for(int i = 0 ; i <100 ; i++)
        {
            if(this.tablica[i])
            {
                indexy.append(i+1);
                indexy.append(" ");
            }
        }
        return indexy.toString();
    }
    public boolean equals(IntegerSet a) {
        for(int i = 0 ; i<100 ; i++) {
            if(a.tablica[i]!=this.tablica[i])
            {
                return false;
            }
        }
        return true;
    }
}