package com.company.labolatorium6;


public class MainLab6 {
    public static void main(String[] args) {
        //Zadanie1
        RachunekBankowy saver1 = new RachunekBankowy(2000.0);
        RachunekBankowy saver2 = new RachunekBankowy(3000.0);
        saver1.setRocznaStopaProcentowa(0.04);
        saver2.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        saver1.setRocznaStopaProcentowa(0.05);
        saver2.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        //Zadanie2
        IntegerSet jeden = new IntegerSet();
        IntegerSet dwa = new IntegerSet();

        jeden.insertElement(1);
        jeden.insertElement(5);
        jeden.insertElement(14);
        jeden.insertElement(16);
        jeden.insertElement(23);
        jeden.insertElement(56);
        jeden.insertElement(99);

        dwa.insertElement(5);
        dwa.insertElement(45);
        dwa.insertElement(75);
        dwa.insertElement(43);
        dwa.insertElement(22);
        //jeden.wypiszTab();
        System.out.println(jeden);
        System.out.println(IntegerSet.union(jeden,dwa));
        System.out.println(jeden.equals(dwa));

        //Zadanie 3
        Pracownik[] personel = new Pracownik[3];
        personel[0] = new Pracownik("Marcin Kapki", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Papaj Nowy", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Mariusz Kozak", 40000, 2005, 3, 15);

        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        for (Pracownik e : personel) {
            System.out.print("Nazwa = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tPobory = " + e.pobory());
            System.out.printf("\tDate = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId();
        System.out.println("Next id = " + n);
    }
}
