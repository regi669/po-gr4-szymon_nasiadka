package com.company.labolatorium6;

public class RachunekBankowy {
    public static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo)
    {
        this.saldo=saldo;
    }
    public void obliczMiesieczneOdsetki() {
        this.saldo += (this.saldo*this.rocznaStopaProcentowa)/12;
    }
    public void setRocznaStopaProcentowa(double rocznaStopaProcentowa) {
        this.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }
    public double getSaldo()
    {
        return this.saldo;
    }
}