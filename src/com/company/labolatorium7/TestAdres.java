package com.company.labolatorium7;

import imiajd.nasiadka.*;

public class TestAdres {
    public static void main(String[] args) {
        Adres a = new Adres("Miejska", 5, "Wadowice", "50-254");
        Adres b = new Adres("Maja", 3, 2, "Olsztyn", "50-243");
        a.pokaz();
        b.pokaz();
        System.out.println(a.przed(b));
        System.out.println(b.przed(a));
    }
}