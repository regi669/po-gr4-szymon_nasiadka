package com.company.labolatorium7;
import imiajd.nasiadka.*;

public class TestNazwanyPunkt
{
    public static void main(String[] args)
    {
        NazwanyPunkt a = new NazwanyPunkt(3, 5, "mak");
        a.show();

        Punkt b = new Punkt(3, 5);
        b.show();

        Punkt c = new NazwanyPunkt(3, 6, "dom");
        c.show();

        a = (NazwanyPunkt) c;
        a.show();
    }
}