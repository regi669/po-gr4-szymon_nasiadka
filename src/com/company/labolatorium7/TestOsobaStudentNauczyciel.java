package com.company.labolatorium7;
import imiajd.nasiadka.*;

public class TestOsobaStudentNauczyciel {
    public static void main(String[] args) {
        Osoba a=new Osoba("Nowak", 2000);
        Student b=new Student("Pakir", 1999, "Matematykaw");
        Nauczyciel c=new Nauczyciel("Czaro", 1564, 2500);
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(a.getNazwisko());
        System.out.println(b.getKierunek());
        System.out.println(c.getPensja());
    }
}