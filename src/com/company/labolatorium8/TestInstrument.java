package com.company.labolatorium8;

import javax.xml.crypto.Data;
import java.lang.reflect.Array;
import java.util.*;
import imiajd.nasiadka.*;
import java.util.ArrayList;
import java.time.LocalDate;

public class TestInstrument
{
    public static void main(String[] args)
    {
        ArrayList<Instrument> Orkiestra = new ArrayList<>();
        Orkiestra.add(0, new Flet("Yamaha", LocalDate.of(2000, 5, 21), "Fuuu"));
        Orkiestra.add(1, new Skrzypce("Koniczila", LocalDate.of(1899, 6, 28), "Skrzyt"));
        Orkiestra.add(2 ,new Fortepian("Margonem", LocalDate.of(1997, 1, 12), "Forttt"));
        Orkiestra.add(3, new Flet("Petylezom", LocalDate.of(321, 1, 15), "Fiiii"));
        Orkiestra.add(4, new Fortepian("Parazom", LocalDate.of(543, 4, 18), "Fortaaam"));

        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.println(Orkiestra.get(i).getDzwiek());
            System.out.println(Orkiestra.get(i).toString());
        }
    }
}