package com.company.labolatorium8;

import javax.xml.crypto.Data;
import java.util.*;
import imiajd.nasiadka.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Pracownik2 c_p1=new Pracownik2("Nowak", new String[]{"Patryk", "Marek"}, LocalDate.of(1999, 4,30), true, 300.50, LocalDate.of(2011, 4, 13));
        Student2 c_s1=new Student2("Podlaska", new String[]{"Joanna"}, LocalDate.of(1985, 1,15), false, "Bezpieczeństwo Wewnętrzene", 4.0);
        System.out.println(c_p1.getNazwisko());
        for(String i : c_p1.getImiona())
        {
            System.out.print(i + " ");
        }
        System.out.println(c_p1.getDataUrodzenia());
        System.out.println(c_p1.getPlec());
        System.out.println(c_p1.getPobory());
        System.out.println(c_p1.getDataZatrudnienia());
        System.out.println(c_p1.getOpis());
        for(String i : c_s1.getImiona())
        {
            System.out.print(i + " ");
        }
        System.out.println(c_s1.getPlec());
        System.out.println(c_s1.getKierunek());
        System.out.println(c_s1.getSredniaOcen());
        c_s1.setSredniaOcen(4.7);
        System.out.println(c_s1.getOpis());
    }
}