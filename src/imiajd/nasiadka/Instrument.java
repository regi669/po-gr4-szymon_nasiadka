package imiajd.nasiadka;

import java.time.LocalDate;

public abstract class Instrument
{
    public Instrument(String producent, LocalDate rokProdukcji)
    {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public abstract String getDzwiek();

    private String producent;
    private LocalDate rokProdukcji;

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Producent: "+this.producent+" Data produkcji: "+this.rokProdukcji+"\n";
    }

    public boolean equals(Object obj)
    {
        return this.toString().equals(obj.toString());
    }
}
