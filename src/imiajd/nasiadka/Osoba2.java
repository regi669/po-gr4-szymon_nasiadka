package imiajd.nasiadka;
import java.time.LocalDate;

public abstract class Osoba2
{
    public Osoba2(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.DataUrodzenia = DataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();
    //public abstract double getPobory();
    //public abstract LocalDate getDataZatrudnienia();

    public String getNazwisko()
    {
        return nazwisko;
    }
    public String[] getImiona()
    {
        return imiona;
    }
    public String getPlec()
    {
        return plec ? "Mężczyzna" : "Kobieta";
    }
    public java.time.LocalDate getDataUrodzenia()
    {
        return DataUrodzenia;
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate DataUrodzenia;
    private boolean plec;
}