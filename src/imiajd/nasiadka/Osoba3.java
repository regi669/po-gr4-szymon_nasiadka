package imiajd.nasiadka;
import java.time.LocalDate;

public class Osoba3 implements Comparable<Osoba3>, Cloneable
{
    public Osoba3(String nazwisko, LocalDate DataUrodzenia)
    {
        this.nazwisko = nazwisko;
        this.DataUrodzenia = DataUrodzenia;
    }

    public String getNazwisko()
    {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia()
    {
        return DataUrodzenia;
    }

    @Override
    public int compareTo(Osoba3 other) {
        int compare_nazwisko = this.nazwisko.compareTo(other.nazwisko);
        if(compare_nazwisko==0){
            return this.DataUrodzenia.compareTo(other.DataUrodzenia);
        }
        return compare_nazwisko;
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+" Nazwisko: "+this.nazwisko+" Data Urodzenia: "+this.DataUrodzenia+"\n";
    }
    @Override
    public boolean equals(Object obj) {
        Osoba3 osb = (Osoba3) obj;
        return (osb.nazwisko.equals(this.nazwisko) && osb.DataUrodzenia.equals(this.DataUrodzenia));
    }


    private String nazwisko;
    private LocalDate DataUrodzenia;
}