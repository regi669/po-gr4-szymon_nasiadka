package imiajd.nasiadka;
import imiajd.nasiadka.Osoba2;

import java.time.LocalDate;

public class Student2 extends Osoba2
{
    public Student2(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, DataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen()
    {
        return sredniaOcen;
    }

    public String getKierunek()
    {
        return kierunek;
    }

    public void setSredniaOcen(double ocena)
    {
        this.sredniaOcen = ocena;
    }

    private String kierunek;
    private double sredniaOcen;
}