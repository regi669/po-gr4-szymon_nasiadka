package imiajd.nasiadka;

import java.time.LocalDate;

public class Student3 extends Osoba3 implements Cloneable, Comparable<Osoba3> {

    private double sredniaOcen;

    public Student3(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }


    public int compareTo(Osoba3 o) {
        int ostatni = super.compareTo((o));
        if((o instanceof Student3) && (ostatni == 0)){
            return -(int)Math.ceil(this.sredniaOcen-((Student3) o).sredniaOcen);
        }
        return ostatni;
    }

    @Override
    public String toString() {
        return getClass().getName() + " nazwisko = " + getNazwisko() + ", data Urodzenia =  " + getDataUrodzenia() + ", srednia ocen= "+ sredniaOcen + "" ;

    }
}